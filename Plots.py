import numpy as np
import matplotlib.pyplot as plt
import Basics


#_____________________________LÖSUNG PLOTTEN______________________________
#Input: FEM Lsg(Koeffizientenvektor), Anzahl der zusätzlichen Auswertungspunkt pro Interval, Polynomordnung, mesh, n
def PlotSolution(alpha,amount,Order,mesh,n):
	domain = np.zeros(amount*n-(n-1))		#amount*n -> Inneren Knoten doppelt gezählt
	values = np.zeros(amount*n-(n-1))
	temp = np.zeros(n+1)
	FuncBasis = np.zeros((amount-1,Order+1))
	step = (mesh[1]-mesh[0])/(amount-1)
	values[-1] = alpha[-1]
	temp = ExtractLinearCoeff(n,alpha,Order)
	for i in range(amount*n-(n-1)):
		domain[i] = mesh[0]+i*step
	RefDomain = np.zeros(amount)
	RefStep = 2/(amount-1)	
	for i in range(0,amount):
		RefDomain[i] = -1+i*RefStep	
	FuncBasis = np.transpose(Basics.RefBasisEvaluationMatrix(RefDomain[1:amount-1],Order))
	#Elementenschleife	
	for i in range(0,n):	
		#Setze innere Koeffizienten
		values[(i*(amount-1))+1:((i+1)*(amount-1))] = np.dot(FuncBasis,alpha[(i*Order):((i+1)*Order+1)])
		#Setzt Knotenkoeffizienten
		values[i*(amount-1)] = temp[i] 
	plt.plot(domain,values)
	plt.show()


#___________________________KONVERGENZORDNUNG PLOTTEN____________________________
def PlotOrder(m,fa,fb,ga,gb,f,c,Solution,Order,QuadOrder,PlotOrder,Sobolev):
	L2Error = np.zeros(m)
	H1Error = np.zeros(m)
	domain = np.zeros(m)
	temp = np.zeros(m)
	for i in range (0,m):
		n = np.power(10,i)
		mesh = Basics.Mesh(n)
		MassMatrix = Basics.CalcGenMassMatrix(mesh,n,Order,QuadOrder,c)
		StiffMatrix = Basics.CalculateStiffMatrix(mesh,n,Order)      
		LGS = Basics.AddMatrices(StiffMatrix,MassMatrix)
		RHS = Basics.CalcRHS(mesh,n,Order,QuadOrder,f)
		RHS = Basics.SetRhsNeumannLeft(RHS,ga)
		RHS = Basics.SetRhsNeumannRight(RHS,gb)
		alpha = Basics.CalcCoefficent(LGS,RHS)
		beta = Basics.SolveMassProblem(mesh,n,Order,QuadOrder,Solution)
		if (Sobolev == 1):
			H1Error[i] = Basics.CalcErrorH1(alpha,beta,MassMatrix,StiffMatrix)
		else:
			L2Error[i] = Basics.CalcErrorL2(alpha,beta,MassMatrix)
		#Definitionsbereich
		domain[i] = np.abs(mesh[1]-mesh[0])
	
	if (Sobolev == 1):	
		plt.loglog(domain,H1Error)
	else:
		plt.loglog(domain,L2Error)
	plt.loglog(domain,np.power(domain,PlotOrder))
	plt.show()


def PlotOrderSparse(m,fa,fb,ga,gb,f,c,Solution,Order,QuadOrder,PlotOrder,Sobolev):
	L2Error = np.zeros(m)
	H1Error = np.zeros(m)
	domain = np.zeros(m)
	temp = np.zeros(m)
	for i in range (0,m):
		n = np.power(10,i)
		mesh = Basics.Mesh(n)
		MassMatrix = SparseBasics.CalcGenMassMatrixSparse(mesh,n,Order,QuadOrder,c)
		StiffMatrix = Basics.CalcStiffMatrixSparse(mesh,n,Order)      
		LGS = Basics.AddMatrices(StiffMatrix,MassMatrix)
		RHS = Basics.CalcRHS(mesh,n,Order,QuadOrder,f)
		RHS = Basics.SetRhsNeumannLeft(RHS,ga)
		RHS = Basics.SetRhsNeumannRight(RHS,gb)
		alpha = Basics.CalcCoefficentSparse(LGS,RHS)
		beta = Basics.SolveMassProblem(mesh,n,Order,QuadOrder,Solution)
		if (Sobolev == 1):
			H1Error[i] = SparseBasics.CalcSparseErrorH1(alpha,beta,MassMatrix,StiffMatrix)
		else:
			L2Error[i] = SparseBasics.CalcSparseErrorL2(alpha,beta,MassMatrix)
		#Definitionsbereich
		domain[i] = np.abs(mesh[1]-mesh[0])
	
	if (Sobolev == 1):	
		plt.loglog(domain,H1Error)
	else:
		plt.loglog(domain,L2Error)
	plt.loglog(domain,np.power(domain,PlotOrder))
	plt.show()


#________________________________SONSTIGES_______________________________________
#Um zu überprüfen, ob Basisfunktionen passen
def PlotFunction(amount,Order,mesh,n):
	domain = np.zeros(amount*n-(n-1))
	values = np.zeros(amount*n-(n-1))
	step = (mesh[1]-mesh[0])/(amount-1)
	values[-1] = Basics.EvalBasis(mesh[n],mesh,n,Order)[1]
	for i in range(amount*n-(n-1)):
		domain[i] = mesh[0]+i*step
	for i in range(0,n):
		#Alle Knoten innerhalb eines Intervalls durchgehen
		for j in range(0,amount-1):
			values[(i*(amount-1))+j] = Basics.EvalBasis(mesh[i]+step*j,mesh,n,Order)[3]	#beliebige Funktion setzen
	plt.plot(domain,values)
	plt.show()


#Zum Testen von High Order Fem
def ExtractLinearCoeff(n,alpha,Order):
	temp = np.zeros(n+1)
	for i in range (0,n+1):
		temp[i] = alpha[i*Order]	
	return temp

