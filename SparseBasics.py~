import numpy as np
import scipy.sparse
import scipy.sparse.linalg
import Basics
import Quadratur as Quad

#if (Order == 1) or (Order == 2):
	#	Anz = (Order + 1) * (Order +1)
#if (Order == 3):
	#	Anz = (Order + 1) * (Order +1) -2
#if (Order > 3):
	#	Anz = Order + 1 + 2*(Order-1) + 6


#___________________MASSE/STEIFIGKEITS-MATRIZEN (c(x) = const)__________________
def ReferenceMatrixMass(Order):
	RefMatrix = scipy.sparse.coo_matrix(Basics.ReferenceMatrixMass(Order))
	return RefMatrix


def ReferenceMatrixStiff(Order):
	RefMatrix = scipy.sparse.coo_matrix(Basics.ReferenceMatrixStiff(Order))
	return RefMatrix


def CalcMassMatrixSparse(mesh,n,k,Order):
	RefMatrix = ReferenceMatrixMass((Order+1),(Order+1))
	Anz = RefMatrix.nnz 	#Anzahl an Elementen
	Entries = np.zeros((n-1)*(Anz-1) + Anz)
	Cols = np.zeros((n-1)*(Anz-1) + Anz)
	Rows = np.zeros((n-1)*(Anz-1) + Anz)
	for i in range(0,n):
		for j in range(0,Anz):
			Entries[j + i*(Anz-1)] =  Entries[j + i*(Anz-1)] +  RefMatrix.data[j]
			Cols[j + i*(Anz-1)] = RefMatrix.col[j] + Order*i
			Rows[j + i*(Anz-1)] = RefMatrix.row[j] + Order*i
	MassMatrix = scipy.sparse.csr_matrix((Entries,(Cols, Rows)), shape=(n*Order+1, n*Order +1)) #Speicherung Im CRS Formaz mittels Koordinaten 
	return  (np.abs(mesh[1]-mesh[0]))*0.5*k*MassMatrix



def CalcStiffMatrixSparse(mesh,n,Order):
	RefMatrix = ReferenceMatrixStiff(Order)
	Anz = RefMatrix.nnz
	Entries = np.zeros((n-1)*(Anz-1) + Anz)
	Cols = np.zeros((n-1)*(Anz-1) + Anz)
	Rows = np.zeros((n-1)*(Anz-1) + Anz)
	for i in range(0,n):
		for j in range(0,Anz):
			Entries[j + i*(Anz-1)] =  Entries[j + i*(Anz-1)] +  RefMatrix.data[j]
			Cols[j + i*(Anz-1)] = RefMatrix.col[j] + Order*i
			Rows[j + i*(Anz-1)] = RefMatrix.row[j] + Order*i
	StiffMatrix = scipy.sparse.csr_matrix((Entries,(Cols, Rows)), shape=(n*Order+1, n*Order +1))
	return 2/(np.abs(mesh[1] - mesh[0]))*StiffMatrix


#____________________________________ALLGEMEINE MASSE MATRIX____________________________________
#Berechnung der ElementMatrix via B^T * D * B (BDB Integrator)
def CalcElementMatrixSparse(mesh,n,Order,QuadOrder,Weights,Vertices,B,c,i):
	TransformedVertices = Quad.VertexTransformation(mesh[i],mesh[i+1],Vertices)
	D = Basics.ElementDiagMatrix(TransformedVertices,Weights,c)
	ElementMatrix = np.dot(B, np.dot(D, np.transpose(B)))
	NullMatrix = CalcNullMatrix(Order)
	ElementMatrix = np.add(ElementMatrix,NullMatrix)
	return scipy.sparse.coo_matrix(ElementMatrix)


def CalcGenMassMatrixSparse(mesh,n,Order,QuadOrder,c):
	[Weights,Vertices] = Quad.AssembleQuadratur(QuadOrder)
	B = Basics.RefBasisEvaluationMatrix(Vertices,Order)
	ElementMatrix = CalcElementMatrixSparse(mesh,n,Order,QuadOrder,Weights,Vertices,B,c,0)# Berechnung auf bel Intervall, um Anz der Einträge zu bestimmen
	Anz = (Order+1)*(Order+1)		#Nonzero Elements
	Entries = np.zeros((n-1)*(Anz-1) + Anz)
	Cols = np.zeros((n-1)*(Anz-1) + Anz)
	Rows = np.zeros((n-1)*(Anz-1) + Anz)
	for i in range(0,n):
		ElementMatrix = CalcElementMatrixSparse(mesh,n,Order,QuadOrder,Weights,Vertices,B,c,i)
		for j in range(0,Anz):
			Entries[j + i*(Anz-1)] = Entries[j + i*(Anz-1)] + ElementMatrix.data[j]		#2 Mal Zugriff auf gleiche Komponenten
		for j in range(1,Anz):
			Cols[j + i*(Anz-1)] = ElementMatrix.col[j] + i*Order
			Rows[j + i*(Anz-1)] = ElementMatrix.row[j] + i*Order
	MassMatrix = scipy.sparse.csr_matrix((Entries,(Cols, Rows)), shape=(n*Order+1, n*Order +1)) 	#Speicherung Im CSR Format mittels Koordinaten
	return  np.abs(mesh[0]-mesh[1])*0.5*MassMatrix


#___________________________MATRIX RANDBEDINGUNGEN___________________________
def SetSparseLgsDirichletLeft(A,Order):
	A.data[0] = 1
	for i in range(1,Order+1):
		A.data[i] = 0
	return A


def SetSparseLgsDirichletRight(A,Order):
	dim = np.shape(A.data)[0]-1	
	A.data[-1] = 1
	for i in range(1,Order+1):
		A.data[i] = 0	
	return A


#____________________KOEFFIZIENTENVEKTOR AUFSTELLEN_______________________________
def CalcCoefficentSparse(A,b):
	alpha = scipy.sparse.linalg.spsolve(A,b)
	return alpha


#____________________________FEHLERBERECHNUNG_____________________________________
#L2-Fehler berechnen:
def CalcSparseErrorL2(alpha,beta,MassMatrix):
	temp = MassMatrix.dot(alpha-beta)          
	return np.sqrt(np.abs(np.dot(np.transpose(alpha-beta),temp)))    #sqrt((a-b)^t*MassMatrix*(a-b))


#H1-Fehler berechnen:
def CalcSparseErrorH1(alpha,beta,MassMatrix,StiffMatrix):
	A = AddMatrices(MassMatrix,StiffMatrix)
	temp = A.dot(alpha-beta)
	return np.sqrt(np.abs(np.dot(np.transpose(alpha-beta),temp)))    #sqrt((a-b)^t*(MassMatrix+StiffMatrix)*(a-b))


#____________________________SONSTIGES____________________________________________
def CalcNullMatrix(Order):
	A = np.zeros((Order+1,Order+1))
	return 1e-16*np.ones((Order+1,Order+1))	




