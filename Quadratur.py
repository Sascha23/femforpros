import numpy as np
import numpy.linalg as LA
import matplotlib.pyplot as plt


def CalcGamma(QuadOrder):
	gamma = np.zeros(QuadOrder)
	for i in range(0,QuadOrder):
		gamma[i] = np.sqrt(((i+1)*(i+1))/(4*(i+1)*(i+1)-1))
	return gamma


def AssembleMatrix(QuadOrder,gamma):
	A = np.zeros((QuadOrder+1,QuadOrder+1))
	for i in range(0,QuadOrder):
		A[i,i+1] = gamma[i]
		A[i+1,i] = gamma[i]
	return A

#Input: Quadraturordnung
#Output: Gaußgewichte, Quadraturknoten
def AssembleQuadratur(QuadOrder):
	Eigenvalues = np.zeros(QuadOrder+1)	#EigenValues = Vertices
	Weights = np.zeros(QuadOrder+1)
	Eigenvector = np.zeros((QuadOrder+1,QuadOrder+1))	
	gamma = CalcGamma(QuadOrder)
	A = AssembleMatrix(QuadOrder,gamma)
	[Eigenvalues,Eigenvector] = LA.eig(A)
	for i in range(0,QuadOrder+1):
		Weights[i] = np.power(Eigenvector[0,i]/LA.norm(Eigenvector[:,i]),2)*2
	return [Weights,Eigenvalues]


#Input: Intervall: [a,b], Gaußquadraturknoten
#Output: Gauß-Quadraturknoten auf [a,b]
def VertexTransformation(a,b,Vertices):
	n = np.shape(Vertices)[0]	
	TransVertices = np.zeros(n)	
	for i in range(0,n):
		TransVertices[i] = 1/2*(b+a + Vertices[i]*(b-a))
	return TransVertices

