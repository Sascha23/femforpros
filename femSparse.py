import numpy as np
import Basics
import matplotlib.pyplot as plt
import Plots
import SparseBasics
import scipy.sparse.linalg


#_____________DATEN DER DGL: -(d²u/dx²) + c*u = f___________
#Koeffizientenfunktion c(x)
c = lambda s: np.sin(3*s-1)+np.pi*s*s*-1.5*s +3 #Wilde Koeffizientenfunktion
#RHS f:
f = lambda s: 16*np.pi*np.pi*np.cos(2*np.pi*s) + c(s)*4*np.cos(2*np.pi*s)
#Dirichlet Randbedingung
fa = 4*np.cos(-2*np.pi)
fb = 4*np.cos(2*np.pi)
#Neumann Randbedinungeng
ga = -8*np.pi*np.sin(-2*np.pi) 
gb = -8*np.pi*np.sin(2*np.pi)
#Lösung:
Solution = lambda s: 4*np.cos(2*np.pi*s)


#___________________________FEM DATEN_____________________________
#Anzahl der Intervalle
n = 1000
#Polynomgrad FES
Order = 10
#Quadratur-Ordnug: Muss hinreichend groß gewählt werden		
QuadOrder = 10


#__________________________PLOT DATEN_____________________________
#Anzahl der Stützstellen pro Intervall zum Plotten
amount = 50
#Steigung der Referenzgerade in PlotOrder
PlotOrder = 6
# m = Anzahl der 10er-Potenzen in PlotOrder: FEM wird durchgeführt mit n = 10¹,10²,...,10^m
m = 4
#Bool Variable: if true -> H¹ Fehler plotten, sonst L2
Sobolev = 0
#Plottet L2- bzw. H¹-Konvergenzordung des BVP
#Plots.PlotOrder(m,fa,fb,ga,gb,f,c,Solution,Order,QuadOrder,PlotOrder,Sobolev)



#_______________________FEM-DURCHFÜHRUNG__________________________
#Erstelle mesh
mesh = Basics.Mesh(n)


#Massematrix
SparseMass = SparseBasics.CalcGenMassMatrixSparse(mesh,n,Order,QuadOrder,c)


#StiffMatrix
SparseStiff = SparseBasics.CalcStiffMatrixSparse(mesh,n,Order)


#Erstelle LGS-Matrix
LGS = SparseMass+SparseStiff


#Füge Ranbedingungen hinzu:
#Linke Seite:
#Wenn Neumann Rb erwünscht, Zeile auskommentieren
#Wenn Dirichlet erwünscht, Zeile einkommentieren        
LGS = SparseBasics.SetSparseLgsDirichletLeft(LGS,Order)


#Rechte Seite
#Wenn Neumann Rb erwünscht, Zeile auskommentieren
#Wenn Dirichlet erwünscht, Zeile einkommentieren
LGS = SparseBasics.SetSparseLgsDirichletRight(LGS,Order)


#Erstelle Rechte Seite mit Quadraturformel (ohne RB)
b = Basics.CalcRHS(mesh,n,Order,QuadOrder,f)


#Füge RB in rechte Seite b des LGS ein
#Linke Seite
b = Basics.SetRhsDirichletLeft(b,fa)
#b = Basics.SetRhsNeumannLeft(b,ga)


#Rechte Seite
b = Basics.SetRhsDirichletRight(b,fb)
#b = Basics.SetRhsNeumannRight(b,gb)


#Löse dense LGS
alpha = SparseBasics.CalcCoefficentSparse(LGS,b)


#___________________PLOT-AUSFÜHEUNG_________________________
Plots.PlotSolution(alpha,amount,Order,mesh,n)


