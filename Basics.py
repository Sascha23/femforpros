import numpy as np
import Quadratur as Quad

#________________________________MESH________________________________
#Input: Anzahl der gewünschten Intervalle
#Output: n+1 Knotenpunkte des Meshs
def Mesh(n):
	mesh = np.zeros(n+1)
	for i in range(0,n+1): 
		mesh[i] = -1 + (2*i)/n
	return mesh


#__________________________REFERENZ MATRIZEN_________________________
#Erstellt Referenz-Marix für Masse-Matrix
def ReferenceMatrixMass(Order):
	A = np.zeros((Order+1,Order+1))
	A[0,0] = 2/3
	A[0,Order] = 1/3				
	A[Order,0] = 1/3
	A[Order,Order] = 2/3
	if (Order >= 2):
		A[0,1] = -1/np.sqrt(6)
		A[1,0] = A[0,1]
		A[Order,1] = -1/np.sqrt(6)
		A[1,Order] = A[Order,1]
	if (Order >= 3):
	#Int(b_0/1,b_j) = 0 für j>3
		A[0,2] = 1/(3*np.sqrt(10))
		A[2,0] = A[0,2]
		A[Order,2] = -1/(3*np.sqrt(10))
		A[2,Order] = A[Order,2]
	#Setze Hauptdiagonale
	for i in range(1,Order):
		A[i,i] = 2/((2*(i+2)-1)*(2*(i+2)-5))
	#Setze Nebendiagonalen
	for i in range(2,Order-1):
		A[i-1,i+1] = -1/((2*(i+1)-1)*np.sqrt((2*(i+1)-3)*(2*(i+1)+1)))
		A[i+1,i-1] = A[i-1,i+1]			
	return A


#Erstelle Referenz-Marix für Steifigkeits-Matrix
def ReferenceMatrixStiff(Order):
	A = np.identity(Order+1)
	A[0,0] = 0.5
	A[0,Order] = -0.5
	A[Order,0] = A[0,Order]
	A[Order,Order] = 0.5
	return A


#___________________MASSE/STEIFIGKEITS-MATRIZEN (c(x) = const)__________________
#Berechne dese Mass-Matrix
def CalculateMassMatrix(mesh,n,k,Order):
	MassMatrix = np.zeros((n*Order+1,n*Order+1))    
	RefMatrix = ReferenceMatrixMass(Order)
	for i in range(0,n):
		for j in range(0,Order+1):
			for l in range(0,Order+1):
				MassMatrix[i*Order+j,i*Order+l] = MassMatrix[i*Order+j,i*Order+l] + RefMatrix[j,l]
	return (np.abs(mesh[1]-mesh[0]))*0.5*k*MassMatrix


#Berechne dense Steifigkeits-Matrix
def CalculateStiffMatrix(mesh,n,Order):
	A = np.identity(n*Order+1)
	RefMatrix = ReferenceMatrixStiff(Order)
	A[0,0] = RefMatrix[0,0]
	A[n*Order,n*Order] = RefMatrix[Order,Order]
	for i in range(0,n):
		A[Order+i*Order,i*Order] = RefMatrix[Order,0]
		A[i*Order,Order+i*Order] = RefMatrix[0,Order]
	return (2/(np.abs(mesh[1] - mesh[0])))*A  


#____________________________________ALLGEMEINE MASSE MATRIX____________________________________
#Erstellt Elementmatrizen, mit Übergabe der transformierten Vertices!!
def ElementDiagMatrix(Vertices,Weights,c):
	size = np.shape(Vertices)[0]
	d = np.zeros(size)
	for i in range(0,size):
		d[i] = Weights[i]*c(Vertices[i])
		D = np.diag(d)
	return D


#Berechnung der ElementMatrix via B^T * D * B (BDB Integrator)
def CalcElementMatrix(mesh,n,Order,QuadOrder,Weights,Vertices,B,c,i):
	TransformedVertices = Quad.VertexTransformation(mesh[i],mesh[i+1],Vertices)
	D = ElementDiagMatrix(TransformedVertices,Weights,c)
	ElementMatrix = np.dot(B, np.dot(D, np.transpose(B)))
	return ElementMatrix


def CalcGenMassMatrix(mesh,n,Order,QuadOrder,c):
	MassMatrix = np.zeros((n*Order+1,n*Order+1))
	[Weights,Vertices] = Quad.AssembleQuadratur(QuadOrder)
	B = RefBasisEvaluationMatrix(Vertices,Order)
	for i in range(0,n):
		ElementMatrix = CalcElementMatrix(mesh,n,Order,QuadOrder,Weights,Vertices,B,c,i)
		for j in range(0,Order+1):
			for l in range(0,Order+1):
				MassMatrix[i*Order+j,i*Order+l] = MassMatrix[i*Order+j,i*Order+l] + ElementMatrix[j,l]
	return  np.abs(mesh[1]-mesh[0])*0.5*MassMatrix


#___________________________MATRIX RANDBEDINGUNGEN____________________________
def SetLgsDirichletLeft(A):
	for i in range(1,np.shape(A)[1]):
		A[0,i] = 0
	A[0,0] = 1 
	return A


def SetLgsDirichletRight(A):
	dim = np.shape(A)[1]
	for i in range(0,dim):
		A[-1,i] = 0
		A[-1,-1] = 1
	return A


#__________________________MAPPING AUF REFERENZELEMENT_______________________
#Mapping: [-1,1] -> [a,b]
#gj korrespondiert mit I_j = [x_j-1,x_j]
def g(s,j,mesh):
	return 0.5*(mesh[j]+mesh[j-1] + s*(mesh[j]-mesh[j-1]))
 

#Mapping: [a,b] -> [-1,1]
#gInvers korrespondiert mit I_j = [x_j-1,x_j]
def gInvers(s,j,mesh):
	return (2*s - mesh[j] - mesh[j-1])/(mesh[j] - mesh[j-1])


#Input: s aus [x0,xn], mesh, n
#Output: 1 <= j <= n, sodass s aus [x_j-1,xj]
def getInterval(s,mesh,n):
	for i in range(1,n+1):
		if ((s >= mesh[i-1]) and (s <= mesh[i])):
			return i


#______________________________BASIS AUSWERTUNG_______________________________
#Auswertung der linearen Referenzbasen
def EvalLinearRefBasis(s):
	return [(1-s)/2,(1+s)/2]


#Input: Auswertungspunkt, Legendre-Index
#Output: Lj(s)
def EvalLegendreDyn(s,j):
	Legendre = np.zeros(j+1)
	Legendre[0] = 1
	if (j >= 1):
		Legendre[1] = s
		for i in range(2,j+1):
			Legendre[i] = s*(2*(i-1)+1)/i*Legendre[i-1] - (i-1)/i*Legendre[i-2]
	return Legendre[j]
 

#Input: Auswertungspunkt, Polynomordnung
#Output: [b1(s)...b_order-1(s)]
#Achtung: j >= 2!!!!
def EvalRefBasis(s,Order):
	func = np.zeros(Order-1)    #Linearen Basiselemente extra
	for i in range(0,Order-1):
		func[i] = 1/(np.sqrt(2*(2*(i+1+2)-3)))*(EvalLegendreDyn(s,i+2) - EvalLegendreDyn(s,i+2-2))    #Indexverschiebung, damit func[0]=...
	return func


#Auswertung der Basiselemente
#Input: Auswertungspunkt, mesh, n, Order
#Output: [b0(s),b1(s)...b_order(s)]
def EvalBasis(s,mesh,n,Order):
	l = getInterval(s,mesh,n)
	temp = np.zeros(Order+1)
	temp[0] = EvalLinearRefBasis(gInvers(s,l,mesh))[0]
	temp[Order] = EvalLinearRefBasis(gInvers(s,l,mesh))[1]
	for i in range(0,Order-1):
		temp[i+1] = EvalRefBasis(gInvers(s,l,mesh),Order)[i]
	return temp


#Input: Auswertungspunkte, Polynomordnug
#Output:	[b0(x0)...b0(xm)]
#		[...............]
#		[bn(x0)...bn(xm)]
def RefBasisEvaluationMatrix(Vertices,Order):
	end = np.shape(Vertices)[0]
	FuncBasis = np.zeros((Order+1,end))
	for i in range(0,end):
		FuncBasis[0,i] = EvalLinearRefBasis(Vertices[i])[0]
		FuncBasis[1:Order,i] = EvalRefBasis(Vertices[i],Order)
		FuncBasis[Order,i] = EvalLinearRefBasis(Vertices[i])[1]
	return FuncBasis


#_________________________RHS AUSFSTELLEN__________________________________
#Berechnet RHS
#Input: mesh, Anzahl der Intervalle, Polynomordnung, Quadraturordnung, Funktion (als function handle)
#Output: [Int(f*b0),...,Int(f*b_n*order+1)]
def CalcRHS(mesh,n,Order,QuadOrder,g):
	FuncBasis = np.zeros((Order+1,QuadOrder+1))
	FuncF = np.zeros(QuadOrder+1)
	temp = np.zeros(Order+1)
	RHS = np.zeros(n*(Order-1)+n+1)			
	[Weights,Vertices] = Quad.AssembleQuadratur(QuadOrder)
	FuncBasis = RefBasisEvaluationMatrix(Vertices,Order)
	for i in range(0,n):
		TransformedVertices = Quad.VertexTransformation(mesh[i],mesh[i+1],Vertices)
		for j in range(0,QuadOrder+1):
			FuncF[j] = g(TransformedVertices[j])	
		temp = FuncF*Weights
		RHS[i*Order:(i+1)*Order+1] = RHS[i*Order:(i+1)*Order+1] + (mesh[i+1]-mesh[i])/2*(np.dot(FuncBasis,temp))
	return RHS


#__________________________________RB RHS__________________________________
#Füge Randbedingungen zu RHS hinzu
def SetRhsDirichletLeft(RHS,fa):
	RHS[0] = fa
	return RHS

def SetRhsDirichletRight(RHS,fb):
	RHS[-1] = fb
	return RHS

def SetRhsNeumannLeft(RHS,ga):
	RHS[0] = RHS[0] - ga
	return RHS

def SetRhsNeumannRight(RHS,gb):
	RHS[-1] = RHS[-1] + gb   
	return RHS


#____________________KOEFFIZIENTENVEKTOR AUFSTELLEN_______________________________
#Löse lineares Gleichungssystem für dense Matrix
def CalcCoefficent(A,b):
	alpha = np.linalg.solve(A,b)
	return alpha

#__________________________MASSEPROBLEM___________________________________________
#Löst Masse Problem: L2-Projektion uh = Solution im schwachen Sinn
def SolveMassProblem(mesh,n,Order,QuadOrder,Solution):
	MassMatrix = CalculateMassMatrix(mesh,n,1,Order)
	RHS = CalcRHS(mesh,n,Order,QuadOrder,Solution)
	beta = CalcCoefficent(MassMatrix,RHS)
	return beta

#____________________________FEHLERBERECHNUNG_____________________________________
#L2-Fehler berechnen:
def CalcErrorL2(alpha,beta,MassMatrix):
	temp = np.dot(MassMatrix,(alpha-beta))          
	return np.sqrt(np.abs(np.dot(np.transpose(alpha-beta),temp)))    #sqrt((a-b)^t*MassMatrix*(a-b))


#H1-Fehler berechnen:
def CalcErrorH1(alpha,beta,MassMatrix,StiffMatrix):
	A = AddMatrices(MassMatrix,StiffMatrix)
	temp = np.dot(A,(alpha-beta))
	return np.sqrt(np.abs(np.dot(np.transpose(alpha-beta),temp)))    #sqrt((a-b)^t*(MassMatrix+StiffMatrix)*(a-b))


#____________________________SONSTIGES____________________________________________
#Addiere Matrizern, um endgültiges GLS aufzustellen
def AddMatrices(A,B):
	return np.add(A,B)





